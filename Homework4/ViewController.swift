//
//  ViewController.swift
//  Homework4
//
//  Created by Valentyn Filippov on 8/28/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let firstName = "Valentyn"
        let middleName = "Sergiyovich"
        let nameSurname = "ValentynFilippov" as NSString
        let someNumber = "123335559"
        var testArray: [Int] = [8,5,9,4,8,6,7,9,9,10]
        let stringToTranslate = "ЯЯЯЗЗЗЬЬЬ МОПС МОРДА"
        let someWords: [String] = ["lada", "sedan", "baklazhan", "timati", "loboda"]
        let password = "1Aab%"
        let rudeString = "You're a big fuck and suka"
        
        printMyNameCount(name: firstName)
        print(checkMyMiddleName(middleName: middleName))
        separateTheName(nameSurname: nameSurname)
        print(newReverse(string: middleName))
        addComasLikeCalc(string: someNumber)
        print("Your password strenght is \(checkPasswordStrength(string: password)) out of 5")
        bubbleSortAndNoDups(array: &testArray)
        pseudoTranslation(string: stringToTranslate)
        print("Here are the words containing 'da' -", containsDa(wordsList: someWords))
        mindYourLanguage(string: rudeString)
    }
    
    func printMyNameCount (name : String) {
        print("You have \(name.count) letters in your name")
    }
    
    func checkMyMiddleName (middleName : String) -> String {
        if (middleName.hasSuffix("ich")) || (middleName.hasSuffix("na")) {
            return "You're ich or vna"
        } else {
            return "You have a rare middle name!"
        }
    }
    
    func separateTheName (nameSurname : NSString) {
        let firstName = nameSurname.substring(to: 8)
        let lastName = nameSurname.substring(from: 8)
        let fullName = firstName + " " + lastName
        print(fullName) // I will probably implement more elegant way
    }
    
    func newReverse (string : String) -> String {
        var reversedString = ""
        for character in string {
            reversedString = String(character) + reversedString
        }
        return reversedString
    }
    
    func addComasLikeCalc (string : String) {
        var withCommas = ""
        var numberOfCommas = 0
        for character in newReverse(string: string) {
            withCommas = String(character) + withCommas
            if (withCommas.count == (3 + 4 * numberOfCommas)) {
                if withCommas.count == (string.count + numberOfCommas) {
                    break
                }
                withCommas = "," + withCommas
                numberOfCommas = numberOfCommas + 1
            }
        }
        print(withCommas)
    }
    
    func checkPasswordStrength (string : String) -> Int {
        var passStrenght = 0
        
        let capitalLetterRegEx = ".*[A-Z]+.*"
        let normalLettersRegEx = ".*[a-z]+.*"
        let numberRegEx  = ".*[0-9]+.*"
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        
        let testCapital = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        if testCapital.evaluate(with: string) {
            passStrenght = passStrenght + 1
        }
        
        let testNormal = NSPredicate(format: "SELF MATCHES %@", normalLettersRegEx)
        if testNormal.evaluate(with: string) {
            passStrenght = passStrenght + 1
        }
        
        let testNumber = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        if testNumber.evaluate(with: string) {
            passStrenght = passStrenght + 1
        }
        
        let testSpecial = NSPredicate(format: "SELF MATCHES %@", specialCharacterRegEx)
        if testSpecial.evaluate(with: string) {
            passStrenght = passStrenght + 1
        }
        
        if passStrenght == 4 {
            passStrenght = passStrenght + 1
        }
        
        return passStrenght
    }
    
    func bubbleSortAndNoDups (array :inout [Int]) {
        print("Unsorted array with duplicates \(array)")
        var noDupsArray: [Int] = []
        for value in array {
            if (noDupsArray.contains(value) == false) {
                noDupsArray.append(value)
            }
        }
        
        array = noDupsArray // no duplicates
        
        for _ in 0..<array.count { // sorting starts here
            for i in 1...(array.count - 2) {
                if array[i - 1] > array[i] { // next three lines perform swap.
                    array[i - 1] = array[i - 1] + array[i]
                    array[i] = array[i - 1] - array[i]
                    array[i - 1] = array[i - 1] - array[i]
                }
            }
        }
        print("Sorted array without duplicates \(array)")
    }
    
    func pseudoTranslation (string : String) {
        let dictionary: [String : String] = [
            "М" : "M",
            "О" : "O",
            "Р" : "R",
            "Д" : "D",
            "А" : "A",
            "Я" : "YA",
            "З" : "Z",
            "П" : "P",
            "С" : "S",
            "Ь" : ""
        ]
        
        var translatedString = string
        for character in string {
            if dictionary.keys.contains(String(character)) {
                translatedString = translatedString.replacingOccurrences(of: String(character), with: dictionary[String(character)]!)
            }
        }
        print("Initial string \(string) means \(translatedString)")
    }
    
    func containsDa (wordsList: [String]) -> [String] {
        var wordsWithDa: [String] = []
        for word in wordsList {
            if word.contains("da") {
                wordsWithDa.append(word)
            }
        }
        return wordsWithDa
    }
    
    func mindYourLanguage (string : String) {
        let swearWords: Set<String> = ["fuck","shit","jerk","homo","suka"]
        var stringArr = string.components(separatedBy: " ")
        
        for _ in swearWords {
            for i in 0..<stringArr.count {
                if swearWords.contains(stringArr[i]) {
                    stringArr[i] = "*censored*"
                }
            }
        }
        print("Mind your language! Here is censored text:", stringArr.joined(separator: " "))
    }
}

